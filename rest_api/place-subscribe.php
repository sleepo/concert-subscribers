<?php defined('ABSPATH') or die();

/**
 * @var $wpdb wpdb
 */
global $wpdb;

register_rest_route( 'concert-subscribers/v1', '/places' . '/(?P<place>[\d]+)' . '/subscribe', [
    'methods' => WP_REST_Server::CREATABLE,
    'args' => [
        'place' => [
            'required' => true,
            'sanitize_callback' => function($param) use ( $wpdb )
            {
                $wpdb->query( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}cs_places WHERE place_id = %d LIMIT 1", [
                    $param
                ]) );

                return $wpdb->last_result ? $wpdb->last_result[0] : null;
            },
            'validate_callback' => function($param)
            {
                return (bool)$param;
            }
        ],
        'name' => [
            'sanitize_callback' => function($param)
            {
                return $param ?: null;
            }
        ],
        'email' =>  [
            'required' => true,
            'validate_callback' => function($param)
            {
                return filter_var( $param, FILTER_VALIDATE_EMAIL );
            }
        ]
    ],
    'callback' => function(\WP_REST_Request $request) use ($wpdb)
    {

        $wpdb->query( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}cs_subscribers WHERE place_id = %d AND email = %s", [
            $request->get_param('place')->place_id,
            $request->get_param('email')
        ] ) );
        $result = $wpdb->last_result;

        if ( $result )
        {
            return $result[0];
        }

        $now = new DateTime();

        $wpdb->insert( "{$wpdb->prefix}cs_subscribers", [

            'place_id' => $request->get_param('place')->place_id,
            'email'    => $request->get_param('email'),
            'name'     => $request->get_param('name'),
            'created_at' => $now->format( 'Y-m-d H:i:s' ),
            'updated_at' => $now->format( 'Y-m-d H:i:s' )
        ]);

        $wpdb->query( $wpdb->prepare("SELECT * FROM {$wpdb->prefix}cs_subscribers WHERE id = %d", [
            $wpdb->insert_id
        ]) );

        return $wpdb->last_result ? $wpdb->last_result[0] : null;
    }
]);