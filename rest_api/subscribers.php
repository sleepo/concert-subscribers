<?php defined('ABSPATH') or die();

/**
 * @var $wpdb wpdb
 */
global $wpdb;

register_rest_route( 'concert-subscribers/v1', '/subscribers', [
    'methods' => WP_REST_Server::READABLE,
    'args' => [

    ],
    'permission_callback' => function()
    {
        return current_user_can('manage_options');
    },
    'callback' => function(\WP_REST_Request $request) use ($wpdb)
    {
        $wpdb->query("
				SELECT
					t.*,
					{$wpdb->prefix}cs_places.place_id,
					{$wpdb->prefix}cs_places.fias_aoguid,
					{$wpdb->prefix}cs_places.name as place_name,
					{$wpdb->prefix}cs_place_type_names.name as place_type_name,
					{$wpdb->prefix}cs_place_type_names.after_place_name as place_type_name_after,
					{$wpdb->prefix}cs_regions.name as region_name,
					region_place_type.name as region_type_name,
					region_place_type.after_place_name as region_type_after
				FROM (SELECT COUNT(place_id) as subscribers_in_place, place_id FROM {$wpdb->prefix}cs_subscribers GROUP BY place_id) as t
				JOIN {$wpdb->prefix}cs_places ON ( t.place_id = {$wpdb->prefix}cs_places.place_id )
				JOIN {$wpdb->prefix}cs_regions ON ( {$wpdb->prefix}cs_places.region_place_id = {$wpdb->prefix}cs_regions.place_id )
				JOIN {$wpdb->prefix}cs_place_type_names ON ( {$wpdb->prefix}cs_places.place_type_name_id = {$wpdb->prefix}cs_place_type_names.place_type_name_id  )
				JOIN {$wpdb->prefix}cs_place_type_names AS region_place_type ON ( {$wpdb->prefix}cs_regions.place_type_name_id = region_place_type.place_type_name_id )
			");

        $results  = [];

        foreach ((array)$wpdb->last_result as $row)
        {
            $place = [
                "full_name" => cs_place_full_name($row),
                "subscribers_in_place" => (int)$row->subscribers_in_place,
                "place_id" => (int)$row->place_id,
                "fias_aoguid" => $row->fias_aoguid,
                "place_name" => $row->place_name,
                "place_type_name" => $row->place_type_name,
                "place_type_name_after" => (bool)$row->place_type_name_after,
                "region_name" => $row->region_name,
                "region_type_name" => $row->region_type_name,
                "region_type_after" => (bool)$row->region_type_after,
                "admin_link" => '/wp-admin/admin.php?' . http_build_query([
                        'page' => 'cs-notify',
                        'place_id' => $row->place_id
                    ]),
                "email_link" => '/wp-admin/admin.php?' . http_build_query([
                        'page' => 'cs-emails',
                        'place_id' => $row->place_id
                    ])
            ];

            $results[] = $place;
        }

        return $results;
    }
]);