<?php defined('ABSPATH') or die();

/**
 * @var $wpdb wpdb
 */
global $wpdb;

register_rest_route( 'concert-subscribers/v1', '/places', [
    'methods' => WP_REST_Server::READABLE,
    'args' => [
        's' => [
            'required' => true,
            'validate_callback' => function($param, $request, $key)
            {
                return is_string( $param ) && strlen( $param ) > 2;
            }
        ],
    ],
    'callback' => function(WP_REST_Request $request) use ( $wpdb )
    {

        $like = esc_sql($request->get_param('s')) . '%';

        $wpdb->query("
				SELECT
					{$wpdb->prefix}cs_places.place_id,
					{$wpdb->prefix}cs_places.fias_aoguid,
					{$wpdb->prefix}cs_places.name as place_name,
					{$wpdb->prefix}cs_place_type_names.name as place_type_name,
					{$wpdb->prefix}cs_place_type_names.after_place_name as place_type_name_after,
					{$wpdb->prefix}cs_regions.name as region_name,
					region_place_type.name as region_type_name,
					region_place_type.after_place_name as region_type_after
				FROM {$wpdb->prefix}cs_places
				JOIN {$wpdb->prefix}cs_regions ON ( {$wpdb->prefix}cs_places.region_place_id = {$wpdb->prefix}cs_regions.place_id )
				JOIN {$wpdb->prefix}cs_place_type_names ON ( {$wpdb->prefix}cs_places.place_type_name_id = {$wpdb->prefix}cs_place_type_names.place_type_name_id  )
				JOIN {$wpdb->prefix}cs_place_type_names AS region_place_type ON ( {$wpdb->prefix}cs_regions.place_type_name_id = region_place_type.place_type_name_id )
				WHERE {$wpdb->prefix}cs_places.name LIKE '{$like}' AND {$wpdb->prefix}cs_place_type_names.code IN (103, 401, 621, 630)
				ORDER BY {$wpdb->prefix}cs_places.level ASC, {$wpdb->prefix}cs_places.is_region_center DESC, {$wpdb->prefix}cs_places.is_center DESC
				LIMIT 100
			");

        $result = [];

        foreach ( (array)$wpdb->last_result as $row )
        {
            $result[] = [
                'full_name' => cs_place_full_name($row),
                'place_id' => (int)$row->place_id,
                'fias_aoguid' => $row->fias_aoguid,
                'place_name' => $row->place_name,
                'place_type_name' => $row->place_type_name,
                'place_type_name_after' => (bool)$row->place_type_name_after,
                'region_name' => $row->region_name,
                'region_type_name' => $row->region_type_name,
                'region_type_after' => (bool)$row->region_type_after
            ];
        }

        return $result;

    },
] );