<?php defined('ABSPATH') or die();

/**
 * @var $wpdb wpdb
 */
global $wpdb;

register_rest_route('concert-subscribers/v1', '/notify', [
    'methods' => WP_REST_Server::CREATABLE,
    'args' => [
        'place'   => [
            'required' => true,
            'sanitize_callback' => function($param) use ( $wpdb )
            {
                $wpdb->query( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}cs_places WHERE place_id = %d LIMIT 1", [
                    $param
                ]) );

                return $wpdb->last_result ? $wpdb->last_result[0] : null;
            },
            'validate_callback' => function($param)
            {
                return (bool)$param;
            }
        ],
        'notify_in_region' => [
            'required' => true,
            'sanitize_callback' => function($param)
            {
                return (bool)$param;
            },
        ],
        'subject' => [
            'required' => true,
            'validate_callback' => function($param)
            {
                return strlen($param) > 0;
            }
        ],
        'message' => [
            'required' => true,
            'validate_callback' => function($param)
            {
                return strlen($param) > 0;
            }
        ],
    ],
    'permission_callback' => function()
    {
        return current_user_can('manage_options');
    },
    'callback' => function(WP_REST_Request $request) use ($wpdb)
    {
        $notify_in_region = $request->get_param('notify_in_region');
        $place = $request->get_param('place');
        $subject = $request->get_param('subject');
        $message = $request->get_param('message');

        if ( $notify_in_region )
        {
            $wpdb->query( $wpdb->prepare("
							SELECT {$wpdb->prefix}cs_subscribers.*
							FROM {$wpdb->prefix}cs_subscribers
							JOIN {$wpdb->prefix}cs_places ON ( wp_cs_places.place_id = wp_cs_subscribers.place_id )
							WHERE {$wpdb->prefix}cs_places.region_place_id = %d
						", [
                $place->region_place_id
            ]) );

            $subscribers = $wpdb->last_result;
        }
        else
        {
            $wpdb->query( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}cs_subscribers WHERE place_id = %s", [
                $place->place_id
            ]) );

            $subscribers = $wpdb->last_result;
        }


        $subscribers_email = [];
        foreach ($subscribers as $subscriber)
        {
            $subscribers_email[] = $subscriber->email;
        }
        $chunked_subscribers_emails = array_chunk( $subscribers_email, 300 );
        $success_recipients = [];
        $failure_recipients = [];

        foreach ($chunked_subscribers_emails as $subscribers_list)
        {
            $num_try = 0;
            $sended = false;

            while ( !$sended && $num_try < 5 )
            {
                $num_try++;

                $sended = wp_mail($subscribers_list, $subject, $message, [
                    'Content-Type: text/html'
                ]);
            }

            if ( $sended )
            {
                foreach ($subscribers_list as $subscriber)
                {
                    $success_recipients[] = $subscriber;
                }
            }
            else
            {
                foreach ($subscribers_list as $subscriber)
                {
                    $failure_recipients[] = $subscriber;
                }
            }

        }

        return [
            'success_recipients' => $success_recipients,
            'failure_recipients' => $failure_recipients,
        ];
    }
]);