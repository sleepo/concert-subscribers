<?php defined('ABSPATH') or die();

/**
 * @var $wpdb wpdb
 */
global $wpdb;

register_rest_route( 'concert-subscribers/v1', '/places/(?P<place>[\d]+)/subscribers', [
    'methods' => WP_REST_Server::READABLE,
    'args' => [
        'place' => [
            'required' => true,
            'sanitize_callback' => 'absint'
        ]
    ],
    'permission_callback' => function()
    {
        return current_user_can('manage_options');
    },
    'callback' => function(WP_REST_Request $request) use ( $wpdb )
    {
        $place_id = $request->get_param('place');

        $wpdb->query(
            $wpdb->prepare("
				SELECT
					{$wpdb->prefix}cs_places.place_id,
					{$wpdb->prefix}cs_places.fias_aoguid,
					{$wpdb->prefix}cs_places.name as place_name,
					{$wpdb->prefix}cs_place_type_names.name as place_type_name,
					{$wpdb->prefix}cs_place_type_names.after_place_name as place_type_name_after,
					{$wpdb->prefix}cs_regions.name as region_name,
					region_place_type.name as region_type_name,
					region_place_type.after_place_name as region_type_after
				FROM {$wpdb->prefix}cs_places
				JOIN {$wpdb->prefix}cs_regions ON ( {$wpdb->prefix}cs_places.region_place_id = {$wpdb->prefix}cs_regions.place_id )
				JOIN {$wpdb->prefix}cs_place_type_names ON ( {$wpdb->prefix}cs_places.place_type_name_id = {$wpdb->prefix}cs_place_type_names.place_type_name_id  )
				JOIN {$wpdb->prefix}cs_place_type_names AS region_place_type ON ( {$wpdb->prefix}cs_regions.place_type_name_id = region_place_type.place_type_name_id )
				WHERE {$wpdb->prefix}cs_places.place_id = %d AND {$wpdb->prefix}cs_place_type_names.code IN (103, 401, 621, 630)
				ORDER BY {$wpdb->prefix}cs_places.level ASC, {$wpdb->prefix}cs_places.is_region_center DESC, {$wpdb->prefix}cs_places.is_center DESC
				LIMIT 1
			", [
                $place_id
            ])
        );

        if (!$wpdb->last_result)
        {
            return new WP_REST_Response(null, 404);
        }

        $tmp = $wpdb->last_result[0];

        $place = [
            'full_name' => cs_place_full_name($tmp),
            'place_id' => (int)$tmp->place_id,
            'fias_aoguid' => $tmp->fias_aoguid,
            'place_name' => $tmp->place_name,
            'place_type_name' => $tmp->place_type_name,
            'place_type_name_after' => (bool)$tmp->place_type_name_after,
            'region_name' => $tmp->region_name,
            'region_type_name' => $tmp->region_type_name,
            'region_type_after' => (bool)$tmp->region_type_after
        ];

        $wpdb->query(
            $wpdb->prepare(
                "SELECT * FROM {$wpdb->prefix}cs_subscribers WHERE place_id = %d ORDER BY created_at DESC",
                [ $place_id ]
            )
        );
        $subscribers = $wpdb->last_result;

        return [
            'data' => [
                'place' => $place,
                'subscribers' => $subscribers,
            ]
        ];

    },
] );