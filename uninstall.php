<?php defined('ABSPATH') or die();

function cs_uninstall_hook()
{
    global $wpdb;

    if ( !get_option('concert_subscribers_activate') )
    {
        return;
    }

    $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}cs_place_type_names`;");
    $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}cs_regions`;");
    $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}cs_places`;");
    $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}cs_subscribers`;");

    update_option('concert_subscribers_activate', false);
}



