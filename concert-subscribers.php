<?php defined('ABSPATH') or die();

/*
Plugin Name: Concert Subscribers
Plugin URI: https://bitbucket.org/sleepo/concert-subscribers
Version: 0.0.7
Author: Daniel Khorkhordin
Author URI: https://bitbucket.org/sleepo/
*/

require('helpers.php');

/*
 * When use relative path, wp try to load wp-admin/install.php instead of ./install.php
 */
require_once(__DIR__ . '/install.php');
register_activation_hook( __FILE__, 'cs_activation_hook' );

require(__DIR__ . '/uninstall.php');
register_uninstall_hook( __FILE__, 'cs_uninstall_hook' );

add_action( 'admin_init', function()
{
    require('admin/init.php');
} );

add_action( 'admin_menu', function()
{
	require('admin/menu.php');
} );

add_action( 'rest_api_init', function ()
{
    require('rest_api/init.php');
} );