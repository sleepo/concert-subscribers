<?php defined('ABSPATH') or die();

$parent_slug = 'cs-overview';

add_menu_page(
    'Подписка на концерты',
    'Подписка на концерты',
    'manage_options',
    $parent_slug,
    function()
    {
        ?>
            <section ng-app="concertSubscribers">
                <div class="wrap" ng-controller="listCtrl">
                    <h1>
                        Подписка на концерты
                    </h1>
                    <table class="wp-list-table widefat fixed striped posts">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Кол-во подписавшихся</th>
                        </thead>

                        <tbody id="the-list" ng-show="loaded">
                        <tr class="iedit hentry" ng-repeat="place in places">
                            <td>
                                <a class="row-title" href="{{place.admin_link}}">{{place.full_name}}</a>
                            </td>
                            <td>
                                {{place.subscribers_in_place}}<br>
                                <a href="{{place.email_link}}">Посмотреть email адреса</a>
                            </td>
                        </tr>
                        <tr>
                            <td ng-hide="places.length" colspan="2" style="text-align: center;">Записей не найдено.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        <?php

    },
    'dashicons-tickets',
    6
);

add_submenu_page(
    $parent_slug,
    'Уведомления',
    'Уведомления',
    'manage_options',
    'cs-notify',
    function()
    {

        ?>
        <section ng-app="concertSubscribers">
            <div class="wrap" ng-controller="notifyCtrl">

                <h1 class="page-title">
                    Отправить уведомления
                </h1>

                <div class="notice notice-success" ng-if="formSuccess">
                    <p>
                        Сообщение отправлено.<br>
                        Писем отправлено: <strong>{{formSuccess.success_recipients}}</strong>
                        Писем не удалось отправить: <strong>{{formSuccess.failure_recipients}}</strong>
                    </p>
                </div>

                <div class="notice notice-error" ng-if="formError">
                    <p>Не удалось отправить сообщение.</p>
                </div>

                <form name="notifyForm" class="clearfix" ng-submit="onSubmit()" ng-if="loaded">
                    <div class="col-9">
                        <div class="form-field">
                            <label class="form-label" for="subject">Тема сообщения:</label>
                            <input type="text" class="form-control" id="subject" ng-model="form.subject" required>
                        </div>
                        <div class="form-field">
                            <label class="form-label" for="message">Текст сообщения:</label>
                            <textarea type="text" class="form-control" id="message" name="message" rows="6" ng-model="form.message" required></textarea>
                        </div>
                        <div class="form-field">
                            <label class="form-label" for="place_id">Город:</label>
                            <ui-select ng-model="form.place" required>
                                <ui-select-match>
                                    <span ng-bind="$select.selected.full_name"></span>
                                </ui-select-match>
                                <ui-select-choices
                                    refresh="refreshPlaces($select.search)"
                                    refresh-delay="250"
                                    repeat="place in places">{{place.full_name}}</ui-select-choices>
                            </ui-select>
                        </div>
                        <label class="checkbox" for="notify_in_region">
                            <input type="checkbox" value="yes" id="notify_in_region" ng-model="form.notify_in_region">
                            Уведомить всех в текущем регионе
                        </label>
                    </div>

                    <div class="col-3">
                        <div class="panel">
                            <div class="panel-footer">
                                <button class="button button-primary button-large" ng-disabled="notifyForm.$invalid || formLocked" type="submit">Отправить</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <?php

    }
);

add_submenu_page(
    $parent_slug,
    'E-mail',
    'E-mail',
    'manage_options',
    'cs-emails',
    function()
    {
        $place_id = isset( $_GET['place_id'] ) ? (int)$_GET['place_id'] : 0;
        ?>
        <section ng-app="concertSubscribers">
            <div class="wrap" ng-controller="subscribersListCtrl" data-place-id='<?= json_encode( $place_id );?>'>
                <div ng-if="loaded">
                    <h1>{{place.full_name}}</h1>
                    <table class="wp-list-table widefat fixed striped posts">
                        <thead>
                        <tr>
                            <th>E-mail</th>
                            <th>Дата</th>
                        </thead>
                        <tbody>
                        <tr ng-repeat="subscriber in subscribers">
                            <td>{{subscriber.email}}</td>
                            <td>{{subscriber.created_at|date}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>

        <?php
    }
);