<?php defined('ABSPATH') or die();

add_action('admin_enqueue_scripts', function($hook)
{
    wp_enqueue_style( 'concert-subscribers-styles', elixir( 'dist/css/plugin.css' ) );
    wp_enqueue_script( 'concert-subscribers-scripts', elixir( 'dist/js/plugin.js' ), [], false, true );
    wp_localize_script( 'concert-subscribers-scripts', 'csSettings', [
        'root' => esc_url_raw( rest_url() ),
        'nonce' => wp_create_nonce( 'wp_rest' )
    ] );
});

