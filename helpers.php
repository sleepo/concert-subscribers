<?php defined('ABSPATH') or die();

function cs_place_full_name(\StdClass $stdClass)
{
    $parts = [];

    if ( $stdClass->region_type_after )
    {
        $parts[] = $stdClass->region_name;
        $parts[] = $stdClass->region_type_name . '.';
    }
    else
    {
        $parts[] = $stdClass->region_type_name . '.';
        $parts[] = $stdClass->region_name;
    }

    if ( $stdClass->place_type_name_after )
    {
        $parts[] = $stdClass->place_name;
        $parts[] = $stdClass->place_type_name . '.';
    }
    else
    {
        $parts[] = $stdClass->place_type_name . '.';
        $parts[] = $stdClass->place_name;
    }

    return sprintf('%s %s, %s %s', $parts[0], $parts[1], $parts[2], $parts[3] );
}

if ( !function_exists('elixir') )
{
    function elixir( $path )
    {
        static $manifest;

        if ( is_null($manifest) )
        {
            $manifest = @file_get_contents( __DIR__ . '/build/rev-manifest.json' );

            if ( $manifest )
            {
                $manifest = json_decode( $manifest, true );
            }

            $manifest = is_array($manifest) ? $manifest : [];
        }

        $found_path = array_key_exists($path, $manifest) ?  'build/' . $manifest[ $path ] : $path;

        return plugin_dir_url( __FILE__ ) . $found_path;
    }
}