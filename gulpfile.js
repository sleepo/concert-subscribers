var elixir = require('laravel-elixir');

elixir.config.publicPath = './';
elixir.config.css.outputFolder = 'dist/css';
elixir.config.js.outputFolder = 'dist/js';

elixir(function(mix) {

    mix.less('plugin.less');

    mix.browserify('plugin.js');

    mix.version([ 'dist/css/plugin.css', 'dist/js/plugin.js' ]);

});