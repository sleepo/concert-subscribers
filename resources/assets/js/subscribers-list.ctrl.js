module.exports = ['$scope', '$http', '$element', function($scope, $http, $element){

    $scope.loaded = false;
    $scope.subscribers = [];
    $scope.place = null;

    var place_id = JSON.parse($element[0].getAttribute('data-place-id'));
    if (!place_id){
        return;
    }
    var url = '/wp-json/concert-subscribers/v1/places/' + place_id + '/subscribers';

    $http.get(url)
        .then(function(resp){
            $scope.loaded = true;
            $scope.subscribers = resp.data.data.subscribers;
            $scope.place = resp.data.data.place;
        });

}];