module.exports = ['$scope', '$http', function($scope, $http){

    $scope.loaded = false;
    $scope.places = [];

    $http.get('/wp-json/concert-subscribers/v1/subscribers/')
        .then(function(resp){
            $scope.loaded = true;
            $scope.places = resp.data;
        });

}];