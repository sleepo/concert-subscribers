var angular = require('angular');

var app = angular.module('concertSubscribers', [
    require('ui-select')
]);

app.config(['$httpProvider', function($httpProvider){

    if ( csSettings && csSettings.nonce ){
        $httpProvider.defaults.headers.common['X-WP-Nonce'] = csSettings.nonce;
    }

}]);

app.config(['uiSelectConfig', function(uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
}]);


app.controller('notifyCtrl', require('./notify.ctrl'));
app.controller('listCtrl', require('./list.ctrl'));
app.controller('subscribersListCtrl', require('./subscribers-list.ctrl'));

