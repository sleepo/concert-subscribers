module.exports = ['$scope', '$http', '$location', function($scope, $http, $location){

    $scope.loaded = false;

    $scope.places = [];

    $scope.form = {
        subject: null,
        message: null,
        place: null,
        notify_in_region: null
    };


    var initialPlaceId = /place_id=([\d]*)/.exec( $location.absUrl() );
    if (initialPlaceId){
        $http.get('/wp-json/concert-subscribers/v1/places/' + initialPlaceId[1] + '/')
            .then(function(resp){
                $scope.form.place = resp.data;
                $scope.loaded = true;
            })
            .catch(function(){
                $scope.loaded = true;
            });
    } else {
        $scope.loaded = true;
    }

    var locked_search = null;
    $scope.refreshPlaces = function(search){

        locked_search = search;

        $http.get('/wp-json/concert-subscribers/v1/places', {
            params: {
                s: search
            }
        }).then(function(resp){
            if ( locked_search === search ){
                $scope.places = resp.data;
            }
        });
    };

    $scope.formLocked = false;
    $scope.formSucess = false;
    $scope.formError = false;

    $scope.onSubmit = function(){

        if ($scope.formLocked){
            return;
        }

        $scope.formSuccess = $scope.formError = false;
        $scope.formLocked = true;

        var form = {
            subject: $scope.form.subject,
            message: $scope.form.message,
            place: $scope.form.place ? $scope.form.place.place_id : null,
            notify_in_region: $scope.form.notify_in_region
        };

        $http
            .post('/wp-json/concert-subscribers/v1/notify', form)
            .then(function(resp){
                $scope.formSuccess = {
                    success_recipients: resp.data.success_recipients.length,
                    failure_recipients: resp.data.failure_recipients.length
                };
                $scope.formLocked = false;
            })
            .catch(function(){
                $scope.formError = true;
                $scope.formLocked = false;
            });

    };

}];